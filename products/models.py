from unicodedata import category
from django.db import models


class Product(models.Model):
    title = models.CharField(max_length=255)
    image = models.ImageField(upload_to='products')
    brand = models.CharField(max_length=20, null=True, blank=True)
    barcode = models.CharField(max_length=100, null=True, blank=True)
    price = models.DecimalField(max_digits=5, decimal_places=2, default=0.0)
    active = models.BooleanField(default=True)
    category = models.ForeignKey('categories.Category', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.title

